<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tvdbsw' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '2l1|s%,2RQajJg.LdrFXx0sM.<ptl b*aFJro]~kI4J1]_qSd`p+n~ko`N3~V_SD' );
define( 'SECURE_AUTH_KEY',  '{EXgbB<9*)~[E<#iIYKfv#Ga#z*.75X<b%fVR^%}G!g_wu%+c-d!1K<l[Q~i2mfE' );
define( 'LOGGED_IN_KEY',    '.Ch7K!L.N1anDUU$^.&t- a&Ozm0;Pb;~$HXi/Nk0K3FU j6u6Yy@%UzHTZ]LDB2' );
define( 'NONCE_KEY',        '>_H5ChA1Q)61T|_JmOSUbjfA4N;P4-J-<K8e_D<q`xOVep/ 9;P:;25lwEs`;gU8' );
define( 'AUTH_SALT',        '6>zdmcKfF[o!4T3a4zY3mqmuJ_:<h]rM8)~0N5{F.~Uda&7L`Cv4{^#{ttfHvJV?' );
define( 'SECURE_AUTH_SALT', 'H]T$y$oa3O|yzLdlOMdK[R(Sluw^hvEr ?@8y>K/Y`qYavK~b0V|`:mp0yEx.@2o' );
define( 'LOGGED_IN_SALT',   ')1}nG):@&t<^O{REo-R6v@(#@N{<H+*P3lR@c?(~*y-xkbcwU?3r0} HWEbR*O(%' );
define( 'NONCE_SALT',       ')@y=LL=Wvl=(5! x#rqgOqAVrqz~u14|2v))WZ.wO(5i49`:+34)&yY$b+[2aU$T' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
